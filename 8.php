<?php

$letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];


$dni="20211817";
$letra="q";

// compruebo si el numero de dni es valido
if($dni<0 || $dni>99999999){
   echo "El numero introducido no es valido"; 
}else{
    // si el dni es valido compruebo la letra
    $resto=$dni%23;
    $letraCalculada=$letras[$resto];
    $letra= strtoupper($letra); // colocar la letra en mayusculas
    if($letraCalculada==$letra){
        echo "La letra es correcta";
    }else{
        echo "La letra {$letra} es incorrecta deberia ser {$letraCalculada}";
    }
}



