<?php

$dia=60;

/* utilizando if */

if($dia==1){
    $resultado="Lunes";
}elseif($dia==2){
    $resultado="Martes";
}elseif($dia==3){
    $resultado="Miercoles";
}elseif($dia==4){
    $resultado="Jueves";
}elseif($dia==5){
    $resultado="Viernes";
}elseif($dia==6){
    $resultado="Sabado";
}elseif($dia==7){
    $resultado="Domingo";
}else{
    $resultado="no es dia de la semana";
}

echo $resultado;


/** realiado con un switch **/

switch ($dia){
    case 1:
        $resultado="Lunes";
        break;
    case 2:
        $resultado="Martes";
        break;
    case 3: 
        $resultado="Miercoles";
        break;
    case 4: 
        $resultado="Jueves";
        break;
    case 5: 
        $resultado="Viernes";
        break;
    case 6: 
        $resultado="Sabado";
        break;
    case 7: 
        $resultado="Domingo";
        break;
    default:
        $resultado="No es dia de la semana";
}

echo $resultado;


/** array asociativo **/
$dias=[
    1=>'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
    'Domingo'
];

if(array_key_exists($dia, $dias)){
    $resultado=$dias[$dia];
}else{
    $resultado="No es dia de la semana";
}

echo $resultado;



