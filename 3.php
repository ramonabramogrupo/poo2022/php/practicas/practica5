<?php

$a=20;
$b=["poco","algo","medio","mucho","enorme"];

if($a<10){
    echo $b[0]; // $a=9
}elseif($a<20){
   echo $b[1];  // $a=15
}elseif($a<30){
   echo $b[2];  // $a=29
}elseif($a<40){
   echo $b[3];  // $a=30
}else{
   echo $b[4]; // $a=100
}

// quiero intentar esto con un switch

switch (true){
    case $a<10:
        echo $b[0];
        break;
    case $a<20:
        echo $b[1];
        break;
    case $a<30:
        echo $b[2];
        break;
    case $a<40:
        echo $b[3];
        break;
    default:
        echo $b[4];
        break;
}
